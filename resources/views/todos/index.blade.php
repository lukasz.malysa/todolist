@extends('layout')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>
    <div class="uper">
        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div><br />
        @endif
        <div>
            <a href="{{ route('todos.create')}}" class="btn btn-primary">@lang('todo.btn_add')</a>
        </div>
        <table class="table table-striped">
            <thead>
            <tr>
                <td>@lang('todo.id')</td>
                <td>@lang('todo.externalid')</td>
                <td>@lang('todo.userid')</td>
                <td>@lang('todo.title')</td>
                <td>@lang('todo.completed')</td>
                <td colspan="3">@lang('todo.action')</td>
            </tr>
            </thead>
            <tbody>
            @foreach($todos as $todo)
                <tr>
                    <td>{{$todo->id}}</td>
                    <td>{{$todo->externalid}}</td>
                    <td>{{$todo->userid}}</td>
                    <td>{{$todo->title}}</td>
                    <td>{{$todo->completed}}</td>
                    <td><a href="{{ route('todos/toggle',$todo->id)}}" class="btn btn-primary">Toogle</a></td>
                    <td><a href="{{ route('todos.edit',$todo->id)}}" class="btn btn-primary">Edit</a></td>
                    <td>
                        <form action="{{ route('todos.destroy', $todo->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
            {{ $todos->links() }}
        <div>
@endsection