@extends('layout')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>
    <div class="card uper">
        <div class="card-header">
            @lang('todo.edit')
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <form method="post" action="{{ route('todos.update', $todo->id) }}">
                @method('PATCH')
                @csrf
                <div class="form-group">
                    <label for="externalid">@lang('todo.externalid')</label>
                    <input type="text" class="form-control" name="externalid" value="{{ $todo->externalid }}" />
                </div>
                <div class="form-group">
                    <label for="userid">@lang('todo.userid')</label>
                    <input type="text" class="form-control" name="userid" value={{ $todo->userid }} />
                </div>
                <div class="form-group">
                    <label for="title">@lang('todo.title')</label>
                    <input type="text" class="form-control" name="title" value="{{ $todo->title }}" />
                </div>
                <div class="form-group">
                    <label for="completed">@lang('todo.completed')</label>
                    <select class="form-control" name="completed" value={{ $todo->completed }}>
                        <option value="0">No</option>
                        <option value="1" @if ($todo->completed)selected @endif>Yes</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">@lang('todo.btn_update')</button>
            </form>
        </div>
    </div>
@endsection