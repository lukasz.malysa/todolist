@extends('layout')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>
    <div class="card uper">
        <div class="card-header">
            @lang('todo.add')
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <form method="post" action="{{ route('todos.store') }}">
                <div class="form-group">
                    @csrf
                    <label for="externalid">@lang('todo.externalid')</label>
                    <input type="text" class="form-control" name="externalid"/>
                </div>
                <div class="form-group">
                    <label for="userid">@lang('todo.userid')</label>
                    <input type="text" class="form-control" name="userid"/>
                </div>
                <div class="form-group">
                    <label for="title">@lang('todo.title')</label>
                    <input type="text" class="form-control" name="title"/>
                </div>
                <div class="form-group">
                    <label for="completed">@lang('todo.completed')</label>
                    <select class="form-control" name="completed">
                        <option value="0">@lang('todo.no')</option>
                        <option value="1">@lang('todo.yes')</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">@lang('todo.btn_add')</button>
            </form>
        </div>
    </div>
@endsection