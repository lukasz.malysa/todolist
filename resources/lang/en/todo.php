<?php
/**
 * Created by lmalysa <lukasz.malysa@gmail.com>
 */

return [
    'add' => 'Add new TODO task',
    'edit' => 'Edit TODO task',
    'added' =>  'TODO task was added',
    'updated' =>  'TODO task was updated',
    'toggled' => 'Completion of TODO task was toggled',
    'deleted' => 'TODO task was deleted',
    'id' => 'ID',
    'externalid' => 'External ID',
    'userid' => 'User ID',
    'title' => 'Title',
    'completed' => 'Completed',
    'btn_add' => 'Add',
    'btn_update' => 'Update',
    'no' => 'No',
    'yes' => 'Yes',
    'action' => 'Action'

];