<?php

Route::get('/', function () {
    return view('welcome');
});

Route::resource('todos', 'TodoController');
Route::get('todos/toggle/{id}', ['as' => 'todos/toggle', 'uses' => 'TodoController@toggle']);