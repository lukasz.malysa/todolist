<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Todos;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $todos = Todos::paginate(10);
        return view('todos.index', compact('todos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('todos.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'externalid'=>'integer|unique:todos,externalid|nullable',
            'userid'=>'required',
            'title'=> 'required|string',
            'completed' => 'required|boolean'
        ]);
        $todo = new Todos([
            'externalid'=> $request->get('externalid'),
            'userid' => $request->get('userid'),
            'title'=> $request->get('title'),
            'completed'=> $request->get('completed')
        ]);
        $todo->save();
        return redirect('/todos')->with('success', __('todo.added'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $todo = Todos::find($id);

        return view('todos.edit', compact('todo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'externalid'=> 'integer|unique:todos,externalid,'.$id.'|nullable',
            'userid'=>'required',
            'title'=> 'required|string',
            'completed' => 'required|boolean'
        ]);

        $todo = Todos::find($id);
        $todo->externalid = $request->get('externalid');
        $todo->userid = $request->get('userid');
        $todo->title = $request->get('title');
        $todo->completed = $request->get('completed');
        $todo->save();

        return redirect('/todos')->with('success', __('todo.updated'));
    }

    /**
     * Toogle
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function toggle($id)
    {
        $todo = Todos::find($id);
        $todo->completed = !$todo->completed;
        $todo->save();

        return redirect('/todos')->with(
            'success',
            __('todo.toggled')
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $todo = Todos::find($id);
        $todo->delete();

        return redirect('/todos')->with('success', __('todo.deleted'));
    }
}