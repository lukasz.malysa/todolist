<?php

namespace App\Console\Commands;

use App\Todos;
use Illuminate\Console\Command;

class SyncTodos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:synctodos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronising TODO list';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = 'https://jsonplaceholder.typicode.com/todos';
        $json = file_get_contents( $file);
        $todos = json_decode($json);

        foreach ($todos as $data) {

            $todo = Todos::where('externalid',$data->id)->first();

            if ($todo) {
                $todo->userid = $data->userId;
                $todo->title = $data->title;
                $todo->completed = $data->completed;
            } else {

                $todo = new Todos([
                    'externalid' => $data->id,
                    'userid' => $data->userId,
                    'title' => $data->title,
                    'completed' => $data->completed
                ]);
            }
            $todo->save();

        }

    }
}
